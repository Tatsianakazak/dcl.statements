-- DCL.Statements

-- 1.Create a new user with the username "rentaluser" and the password "rentalpassword". Give the user the ability to connect to the database but no other permissions.
CREATE USER rentaluser
WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental2 TO rentaluser;

-- 2.Grant "rentaluser" SELECT permission for the "customer" table. Сheck to make sure this permission works correctly—write a SQL query to select all customers.

GRANT SELECT ON TABLE customer TO rentaluser;

-- run via psql
-- psql -h localhost -U rentaluser -d dvdrental

SELECT *
FROM customer;

-- 3.Create a new user group called "rental" and add "rentaluser" to the group. 

CREATE ROLE rental
NOLOGIN;
GRANT rental TO rentaluser;

-- 4.Grant the "rental" group INSERT and UPDATE permissions for the "rental" table. Insert a new row and update one existing row in the "rental" table under that role. 


GRANT INSERT, UPDATE ON TABLE rental TO rental;

-- run via psgl
-- psql -h localhost -U rentaluser -d dvdrental

INSERT INTO rental
    (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES
    (CURRENT_TIMESTAMP, 1, 1, CURRENT_TIMESTAMP + INTERVAL
'3 days', 1, CURRENT_TIMESTAMP);


UPDATE rental
SET return_date = CURRENT_TIMESTAMP + INTERVAL
'5 days'
WHERE rental_id = 1;


-- 5.Revoke the "rental" group's INSERT permission for the "rental" table. Try to insert new rows into the "rental" table make sure this action is denied.

REVOKE INSERT ON TABLE rental FROM rental;

-- run via psql
-- psql -h localhost -U rentaluser -d dvdrental

INSERT INTO rental
    (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES
    (CURRENT_TIMESTAMP, 2, 2, CURRENT_TIMESTAMP + INTERVAL
'3 days', 1, CURRENT_TIMESTAMP);

-- 6.Create a personalized role for any customer already existing in the dvd_rental database. The name of the role name must be client_{first_name}_{last_name} (omit curly brackets). The customer's payment and rental history must not be empty. Configure that role so that the customer can only access their own data in the "rental" and "payment" tables. Write a query to make sure this user sees only their own data.
DO $$
DECLARE
    customer_rec RECORD;
    role_name TEXT;
    rental_view_name TEXT;
    payment_view_name TEXT;
BEGIN
    FOR customer_rec IN
    SELECT customer_id, first_name, last_name
    FROM customer
    WHERE customer_id IN (SELECT customer_id
        FROM payment)
        AND customer_id IN (SELECT customer_id
        FROM rental)
    LOOP
        
        role_name := 'client_' || customer_rec.first_name || '_' || customer_rec.last_name;
EXECUTE 'CREATE ROLE '
|| quote_ident
(role_name) || ' WITH LOGIN PASSWORD ''password''';

       
        rental_view_name := 'rental_view_' || customer_rec.first_name || '_' || customer_rec.last_name;
EXECUTE 'CREATE VIEW '
|| quote_ident
(rental_view_name) || ' AS ' ||
                'SELECT * FROM rental WHERE customer_id = ' || customer_rec.customer_id;

  
        payment_view_name := 'payment_view_' || customer_rec.first_name || '_' || customer_rec.last_name;
EXECUTE 'CREATE VIEW '
|| quote_ident
(payment_view_name) || ' AS ' ||
                'SELECT * FROM payment WHERE customer_id = ' || customer_rec.customer_id;


EXECUTE 'GRANT SELECT ON '
|| quote_ident
(rental_view_name) || ' TO ' || quote_ident
(role_name);
EXECUTE 'GRANT SELECT ON '
|| quote_ident
(payment_view_name) || ' TO ' || quote_ident
(role_name);
END LOOP;
END $$;